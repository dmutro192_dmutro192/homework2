class item:
    def __init__(self, n, p, q):
        self.name = n
        self.price = p
        self.quantity = q

    def show(self):
        z = (str(self.name))
        print(z)
        self1 =("$")+(str(self.price))
        print(self1)

    def getName(self):
        return self.name

    def getPrice(self):
        return ("$") + str(self.price)

    def getQuantity(self):
        return self.quantity

class shoppingCart:
    def __init__(self, items):
        self.items = []

    def show(self):
        print(self.items)

    def addItem(self, item):
        if item not in self.items:
            self.items.append(item)
        else:
            item.q += 1

    def checkOut (self):
        total = 0
        for i in self.items:
            i.show()

item1 = item("Banana", 5, 3)
item2 = item("Tea", 15, 2)
c = shoppingCart([])
c.addItem(item1)
c.addItem(item2)
c.show()
print ("You have 2 items in your cart for a total of", (c.checkOut()))


